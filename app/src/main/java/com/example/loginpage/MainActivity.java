package com.example.loginpage;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
public class MainActivity extends AppCompatActivity {
    private TextInputLayout LoginEmail;
    private TextInputLayout LoginPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LoginEmail = findViewById(R.id.input_email);
        LoginPassword = findViewById(R.id.input_password);
    }

    private boolean ValidateEmail() {
        String LoginEmailInput = LoginEmail.getEditText().getText().toString().trim();
        if (LoginEmailInput.isEmpty()) {
            LoginEmail.setError("Field Can't be empty");
            return false;
        }

        else if (!(LoginEmailInput.matches("saudjutt05@gmail.com"))) {
            LoginEmail.setError("invalid Email Address");
            return false;
        } else {
            LoginEmail.setError(null);
            LoginEmail.setErrorEnabled(false);
            return true;
        }

    }

    private boolean ValidatePassword() {

        String LoginpasswordInput = LoginPassword.getEditText().getText().toString().trim();
        if (LoginpasswordInput.isEmpty()) {
            LoginPassword.setError("Field Can't be empty");
            return false;
        }
        else if (!(LoginpasswordInput.matches("saudjutt786"))) {
            LoginPassword.setError("Incorrect password");
            return false;
        } else {
            LoginPassword.setError(null);
            LoginPassword.setErrorEnabled(false);
            return true;
        }

    }

    public void confirmInput(View v) {
        if (!ValidateEmail() | !ValidatePassword())
        {
            return;
        }
        Toast.makeText(MainActivity.this, "Success.", Toast.LENGTH_SHORT).show();

    }

}
